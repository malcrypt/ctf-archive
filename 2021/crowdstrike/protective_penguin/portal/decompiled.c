int validate_creds(char *username,char *password)
{
  int ret_value;
  size_t str_length;
  char *result;
  long lVar1;
  undefined8 *puVar2;
  long in_FS_OFFSET;
  FILE *creds_file_handle;
  undefined4 invalid_login;
  char cred_file_contents [256];
  char provided_cred_str [130];
  char *creds_filename;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  lVar1 = 0x42;
  puVar2 = (undefined8 *)&invalid_login;
  while (lVar1 != 0) {
    lVar1 = lVar1 + -1;
    *puVar2 = 0;
    puVar2 = puVar2 + 1;
  }
  invalid_login = 1;
  creds_filename = "creds.txt";
  __b64_pton(username,provided_cred_str,0x100);
  str_length = strlen(provided_cred_str);
  *(undefined2 *)(provided_cred_str + str_length) = 0x3a;
  str_length = strlen(provided_cred_str);
  __b64_pton(password,provided_cred_str + str_length,0x100);
  creds_file_handle = fopen(creds_filename,"r");
  if (creds_file_handle == (FILE *)0x0) {
    ret_value = -1;
  }
  else {
    do {
      do {
        result = fgets(cred_file_contents,0x100,creds_file_handle);
        if (result == (char *)0x0) goto LAB_00401409;
        str_length = strlen(cred_file_contents);
        result = strchr(cred_file_contents,0x3a);
      } while (result == (char *)0x0);
      if ((str_length != 0) && (cred_file_contents[str_length - 1] == '\n')) {
        cred_file_contents[str_length - 1] = '\0';
      }
      ret_value = strcmp(provided_cred_str,cred_file_contents);
    } while (ret_value != 0);
    invalid_login = 0;
LAB_00401409:
    fclose(creds_file_handle);
    ret_value = invalid_login;
  }
  if (local_10 == *(long *)(in_FS_OFFSET + 0x28)) {
    return ret_value;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}

int main(void)
{
  int result;
  uint content_length;
  char *env_var;
  size_t bytes_read;
  long lVar1;
  undefined8 *puVar2;
  long in_FS_OFFSET;
  undefined8 username_obj;
  undefined8 password_obj;
  char *username;
  char *password;
  long parsed_json;
  undefined8 request_data;
  undefined8 local_410;
  undefined8 local_408 [127];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  username = (char *)0x0;
  password = (char *)0x0;
  request_data = 0;
  local_410 = 0;
  lVar1 = 0x7e;
  puVar2 = local_408;
  while (lVar1 != 0) {
    lVar1 = lVar1 + -1;
    *puVar2 = 0;
    puVar2 = puVar2 + 1;
  }
  puts("Content-Type: application/json\r\n\r");
  env_var = getenv("REQUEST_METHOD");
  result = strcmp(env_var,"POST");
  if (result != 0) {
    printf("{\"status\": \"unexpected-method\"}");
    result = -1;
    goto LAB_004016ad;
  }
  env_var = getenv("CONTENT_LENGTH");
  content_length = atoi(env_var);
  if (((int)content_length < 0) || (0x3ff < content_length)) {
    printf("{\"status\": \"invalid-content-length\"}");
    result = -1;
    goto LAB_004016ad;
  }
  bytes_read = fread(&request_data,1,(long)(int)content_length,stdin);
  if (bytes_read != (long)(int)content_length) {
    result = -1;
    goto LAB_004016ad;
  }
  parsed_json = json_tokener_parse(&request_data);
  username_obj = 0;
  password_obj = 0;
  if (parsed_json == 0) {
LAB_00401688:
    printf("{\"status\": \"invalid-json\"}");
  }
  else {
    result = json_object_object_get_ex(parsed_json,&USER,&username_obj);
    if (result == 0) goto LAB_00401688;
    result = json_object_object_get_ex(parsed_json,&PASS,&password_obj);
    if (result == 0) goto LAB_00401688;
    username = (char *)json_object_get_string(username_obj);
    if (username == (char *)0x0) goto LAB_00401688;
    password = (char *)json_object_get_string(password_obj);
    if (password == (char *)0x0) goto LAB_00401688;
    result = validate_creds(username,password);
    if (result == 0) {
      env_var = getenv("FLAG");
      printf("{\"status\": \"success\", \"flag\": \"%s\"}",env_var);
    }
    else {
      printf("{\"status\": \"err\"}");
    }
  }
  json_object_put(parsed_json);
  result = 0;
LAB_004016ad:
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return result;
}
