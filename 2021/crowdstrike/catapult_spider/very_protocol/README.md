# Very Protocol
We were approached by a CATAPULT SPIDER victim that was compromised and had all their cat pictures encrypted. Employee morale dropped to an all-time low. We believe that we identified a binary file that is related to the incident and is still running in the customer environment, accepting command and control traffic on

veryprotocol.challenges.adversary.zone:41414

Can you help the customer recover the encryption key?

[malware](provided/malware)

**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/crowdstrike/catapult_spider/very_protocol/)**