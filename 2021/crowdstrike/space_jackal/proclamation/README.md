# The Proclamation
A mysterious file appeared on a deep dark web forum. Can you figure out what we can't see right now?

NOTE: Flags will be easily identifiable by following the format CS{some_secret_flag_text}. They must be submitted in full, including the CS{ and } parts

[proclamation.dat](provided/proclamation.dat)

# Solution
**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/crowdstrike/space_jackal/proclamation/)**
