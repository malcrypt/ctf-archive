import argparse
import itertools
import sys

PREFIX = b"SPACEARMY"

def recover_key(ciphertext_hex: str) -> None:
    key = bytearray()
    ciphertext = bytearray.fromhex(ciphertext_hex)

    if len(ciphertext) < len(PREFIX):
        print("Ciphertext must be at least 9 bytes long")
        sys.exit(1)

    for i in range(0, 3):
        for k0, k1, k2 in itertools.product(range(256), range(256), range(256)):
            p_i = ((k0 * ciphertext[0]) + (k1 * ciphertext[1]) + (k2 * ciphertext[2])) & 0xff
            p_i3 = ((k0 * ciphertext[3]) + (k1 * ciphertext[4]) + (k2 * ciphertext[5])) & 0xff
            p_i6 = ((k0 * ciphertext[6]) + (k1 * ciphertext[7]) + (k2 * ciphertext[8])) & 0xff

            if (p_i == PREFIX[i]) and (p_i3 == PREFIX[i + 3]) and (p_i6 == PREFIX[i + 6]):
                key.append(k0)
                key.append(k1)
                key.append(k2)
                break
    return key

def decrypt(ciphertext_hex: str, key_str: str) -> str:
    ciphertext = bytearray.fromhex(ciphertext_hex)
    key = key_str.encode("utf-8")
    plaintext = bytearray(len(ciphertext))

    for i in range(0, len(ciphertext), 3):
        plaintext[i] = ((key[0] * ciphertext[i]) + (key[1] * ciphertext[i + 1]) + (key[2] * ciphertext[i + 2])) & 0xff
        plaintext[i + 1] = ((key[3] * ciphertext[i]) + (key[4] * ciphertext[i + 1]) + (key[5] * ciphertext[i + 2])) & 0xff
        plaintext[i + 2] = ((key[6] * ciphertext[i]) + (key[7] * ciphertext[i + 1]) + (key[8] * ciphertext[i + 2])) & 0xff

    if plaintext[:9] != PREFIX:
        print("Invalid decrypt")
        sys.exit(1)

    return plaintext[9:].rstrip(b"\x00").decode("utf-8")


def main():
    parser = argparse.ArgumentParser(description="Cryptanalysis on SPACE JACKAL cryptosystem")
    subparsers = parser.add_subparsers(dest="command", required=True, help="Command to execute")

    key_parser = subparsers.add_parser("key", help="Launch known plaintext attack to recover key")
    key_parser.add_argument("ciphertext", type=argparse.FileType("r"), help="File containing the ciphertext (in hex) generated using SPACE JACKAL cryptosystem")

    decrypt_parser = subparsers.add_parser("decrypt", help="Decrypt given ciphertext with given key")
    decrypt_parser.add_argument("ciphertext", type=argparse.FileType("r"),  help="File containing the ciphertext (in hex) generated using SPACE JACKAL cryptosystem")
    decrypt_parser.add_argument("key", help="Key for SPACE JACKAL cryptosystem")

    args = parser.parse_args()

    if args.command == "key":
        key = recover_key(args.ciphertext.read())

        if key:
            print("Possible Key:")
            print(key.decode("utf-8"))
        else:
            print("Unable to find a possible key!")
    elif args.command == "decrypt":
        plaintext = decrypt(args.ciphertext.read(), args.key)
        print("Plaintext:")
        print(plaintext)

if __name__ == "__main__":
    main()