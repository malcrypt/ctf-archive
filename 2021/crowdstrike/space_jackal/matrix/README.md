# Matrix
With the help of your analysis, we got onto the trail of the group and found their [hidden forum on the Deep Dark Web](http://spacesftw5q4uyamog5qgcf75a5hbhmr2g24jqsdcpjnew63zkq7ueyd.onion/). Unfortunately, all messages are encrypted. While we believe that we have found their encryption tool, we are unsure how to decrypt these messages. Can you assist?

[crypter.py](provided/crypter.py)

## Solution
**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/crowdstrike/space_jackal/matrix/)**