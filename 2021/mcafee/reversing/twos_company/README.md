# Two's Company (400)
File me once, shame on you. File me twice, shame on me!

[crackme](provided/crackme)

**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/mcafee/reversing/twos_company/)**