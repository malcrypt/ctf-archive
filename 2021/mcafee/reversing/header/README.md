# Know your header - Part 2 (300)
When you have solved the second part of "Know your Header", you can enter the flag here.

You will need to have solved "Know your Header (Part I)" for this challenge. Use the binary from Part I, named "challenge_encoded" to solve this.

**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/mcafee/reversing/header/)**