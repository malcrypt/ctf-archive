# Looking for Droids? (100)
The following APK is an application that contains encrypted passwords. Your task, should you choose to accept, is to find the flag in one of these passwords.

[PasswordVault.apk](provided/PasswordVault.apk)

**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/mcafee/mobile/droids/)**