# Password in a Haystack (200)
You have acquired the Strings (*nix command) output for a file. You know that a user's password is somewhere in the file and need to retrieve it. The username is steve557. Find the password based on the password rules. Only one string will match these rules.

- All passwords must be 6-12 printable characters in length.
- Each password must contain at least 3 unique digits
- Passwords cannot contain 3 consecutive characters of your username nor its reverse. This is case insensitive.

Remember, no brute forcing of the site is allowed!

[output.txt](provided/output.txt)

**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/mcafee/forensics/haystack/)**