# A Picture is Worth a Thousand Vulns (200)

![](provided/faWr3Mg.png)

You've come across a popular WiFi range extender at the store and decided to make it your next target. Before putting down the cash to buy the thing, you decide to do some recon at home to see if it's worth your time. After all, a good hacker can find out all sorts of things without even opening the box...

http://challenges.ctfd.io:30463

**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/mcafee/recon/picture/)**