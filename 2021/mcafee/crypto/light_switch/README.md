# Light Switch Crypto (100)
Here is a 3-way light switch. Look closely, it may help you uncover the secret message! Solve the secretly coded message to capture the flag!

[light_switch_crypto.tar](provided/light_switch_crypto.tar)

**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/mcafee/crypto/light_switch/)**