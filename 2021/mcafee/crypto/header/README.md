# Know your header - Part 1 (200)
The challenge was encrypted, but we have the encryption script. Can you figure out the key?

For this challenge, the flag is the key to decrypt the next stage "Know your header (part II)". The challenge will become available if you solve part I.

[know_your_header.tar](provided/know_your_header.tar)

**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/mcafee/crypto/header/)**