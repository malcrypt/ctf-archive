import itertools
import string

COLUMNS = [ord(x) for x in string.ascii_lowercase[:8]]
ROWS = [int(x) for x in string.digits[1:9]]

def parse_matches(chess_matches):
    return [c.split('+') for c in chess_matches.split(' ')]

def next_space(column, row, direction):
    if direction == "N":
        col_diff = 0
        row_diff = 1
    elif direction == "NE":
        col_diff = 1
        row_diff = 1
    elif direction == "E":
        col_diff = 1
        row_diff = 0
    elif direction == "SE":
        col_diff = 1
        row_diff = -1
    elif direction == "S":
        col_diff = 0
        row_diff = -1
    elif direction == "SW":
        col_diff = -1
        row_diff = -1
    elif direction == "W":
        col_diff = -1
        row_diff = 0
    elif direction == "NW":
        col_diff = -1
        row_diff = 1

    new_column = column + col_diff
    new_row = row + row_diff

    if (new_column in COLUMNS) and (new_row in ROWS):
        return new_column, new_row
    else:
        return None

def possible_moves(color, rank, coord):
    column = ord(coord[0])
    row = int(coord[1])
    next_moves = []

    if rank == "q":
        for direction in ["N", "NE", "E", "SE", "S", "SW", "W", "NW"]:
            new_space = next_space(column, row, direction)
            while new_space:
                next_moves.append("{}{}".format(chr(new_space[0]), new_space[1]))
                new_space = next_space(new_space[0], new_space[1], direction)

        return next_moves
    elif rank == "k":
        for direction in ["N", "NE", "E", "SE", "S", "SW", "W", "NW"]:
            new_space = next_space(column, row, direction)
            if new_space:
                next_moves.append("{}{}".format(chr(new_space[0]), new_space[1]))

        return next_moves
    elif rank == "b":
        for direction in ["NE", "SE", "SW", "NW"]:
            new_space = next_space(column, row, direction)
            while new_space:
                next_moves.append("{}{}".format(chr(new_space[0]), new_space[1]))
                new_space = next_space(new_space[0], new_space[1], direction)

        return next_moves
    elif rank == "r":
        for direction in ["N", "E", "S", "W"]:
            new_space = next_space(column, row, direction)
            while new_space:
                next_moves.append("{}{}".format(chr(new_space[0]), new_space[1]))
                new_space = next_space(new_space[0], new_space[1], direction)

        return next_moves
    elif rank == "n":
        for direction in [(1, 2), (2, 1), (2, -1), (1, -2), (-1, -2), (-2, -1), (-2, 1), (-1, 2)]:
            new_column = column + direction[0]
            new_row = row + direction[1]

            if (new_column in COLUMNS) and (new_row in ROWS):
                next_moves.append("{}{}".format(chr(new_column), new_row))

        return next_moves

    elif rank == "p":
        if color == "w":
            for direction in ["NW", "N", "NE"]:
                new_space = next_space(column, row, direction)
                if new_space:
                    next_moves.append("{}{}".format(chr(new_space[0]), new_space[1]))

            if row == 2:
                # If the pawn hasn't moved yet, it can also move 2 spaces
                next_moves.append("{}{}".format(chr(column), row + 2))

            return next_moves
        else:
            for direction in ["SW", "S", "SE"]:
                new_space = next_space(column, row, direction)
                if new_space:
                    next_moves.append("{}{}".format(chr(new_space[0]), new_space[1]))

            if row == 7:
                # If the pawn hasn't moved yet, it can also move 2 spaces
                next_moves.append("{}{}".format(chr(column), row - 2))

            return next_moves

def kings_in_check(chess_match):
    white_pieces = []
    white_king_coord = None
    black_pieces = []
    black_king_coord = None
    
    for piece in chess_match:
        color, rank, coord = piece.split(",")
        if color == "w":
            if rank == "k":
                white_king_coord = coord
            
            white_pieces.append(piece)
        elif color == "b":
            if rank == "k":
                black_king_coord = coord
            
            black_pieces.append(piece)

    for piece in white_pieces:
        color, rank, coord = piece.split(",")
        if black_king_coord in possible_moves(color, rank, coord):
            return True

    for piece in black_pieces:
        color, rank, coord = piece.split(",")
        if white_king_coord in possible_moves(color, rank, coord):
            return True

    return False

def main():
    result = []
    chess_matches = parse_matches(raw_input())
    for chess_match in chess_matches:
        result.append(kings_in_check(chess_match))
        
    print(result)

if __name__ == "__main__":
    main()