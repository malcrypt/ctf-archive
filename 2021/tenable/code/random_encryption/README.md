# Random Encryption (100)
We found the following file on a hacked terminal:

[encrypt.py](encrypt.py)

We also found sample output from a previous run:

[output.txt](output.txt)

Can you decode it?

**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/tenable/code/random_encryption/)**
