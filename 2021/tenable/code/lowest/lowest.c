#include <stdio.h>

void print_lowest(int integer_list[], unsigned int length, unsigned short n_lowest)
{
    unsigned short num_swaps = 1;
	int temp = 0;

    while(num_swaps != 0)
    {
		num_swaps = 0;
        for(int i = 0; i < length - 1; i++)
        {
            if(integer_list[i] > integer_list[i+1])
			{
				temp = integer_list[i];
				integer_list[i] = integer_list[i+1];
				integer_list[i+1] = temp;
				num_swaps += 1; 
			}
        }
    }

	for(int i = 0; i < n_lowest; i++)
	{
		printf("%d", integer_list[i]);
		if(i != n_lowest - 1)
		{
			printf(" ");
		}
	}
}

int main(int argc, char *argv)
{
    char input[0x100];
	int integer_list[0x100];
	unsigned int length;
	unsigned short n_lowest;
	int i = 0;

	scanf("%d", &n_lowest);
	scanf("%d", &length);
	
	for(i; i < length; i++)
	{
	    scanf("%d", &integer_list[i]);
	}
	
	print_lowest(integer_list, length, n_lowest);
}
