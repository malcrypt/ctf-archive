import argparse
import sys

from pwn import xor

def xor_once(a, b):
    if len(a) < len(b):
        a = a + b"\x00" * (len(b) - len(a))
    elif len(b) < len(a):
        b = b + b"\x00" * (len(a) - len(b))
    return xor(a, b)


class Emulator:
    def __init__(self, instructions, state):
        self.state = state
        self.instructions = instructions

    def run(self):
        while self.state["IP"] < len(self.instructions):
            instruction = self.instructions[self.state["IP"]]

            opcode, operands = instruction.split(" ", 1)

            if opcode == "XOR":
                dest, src = operands.split(" ", 1)
                if "\"" in src:
                    src = src.replace("\"", "")
                    self.state[dest] = xor_once(self.state[dest], src.encode("utf-8"))
                elif src in self.state:
                    self.state[dest] = xor_once(self.state[dest], self.state[src])
                else:
                    print(f"Unknown operand to XOR: {src}")
                    sys.exit(1)
                
            elif opcode == "MOV":
                dest, src = operands.split(" ", 1)
                if "\"" in src:
                    src = src.replace("\"", "")
                    self.state[dest] = src.encode("utf-8")
                elif src in self.state:
                    self.state[dest] = self.state[src]
                else:
                    print(f"Unknown operand to MOV: {src}")
                    sys.exit(1)

            elif opcode == "REVERSE":
                self.state[operands] = self.state[operands][::-1]

            else:
                ip = self.state["IP"]
                print(f"Invalid opcode at instruction {ip}: {self.instructions[ip]}")
                sys.exit(1)

            self.state["IP"] += 1

    def print_state(self):
        print("TRX: {}".format(self.state["TRX"]))
        print("DRX: {}".format(self.state["DRX"]))
        print("Instruction Pointer: {}".format(self.state["IP"]))

    def __str__(self):
        return "TRX: {TRX} | DRX: {DRX} | IP: {IP}".format(**self.state)

    def __repr__(self):
        return str(self)


def main():
    parser = argparse.ArgumentParser(description="Emulator for String Decryption Machine")

    parser.add_argument("asm", type=argparse.FileType("r"), help="File containing assembly instructions to execute")
    parser.add_argument("ciphertext", help="Ciphertext to decrypt")

    args = parser.parse_args()

    ciphertext = bytes.fromhex(args.ciphertext)

    instructions = list(filter(None, args.asm.read().split("\n")))
    state = {
        "TRX": ciphertext,
        "DRX": "",
        "IP": 0
    }
    em = Emulator(instructions, state)

    em.run()
    em.print_state()

if __name__ == "__main__":
    main()