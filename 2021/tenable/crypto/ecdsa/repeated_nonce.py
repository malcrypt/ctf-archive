from Crypto.Cipher import AES


def main():
    # Reference: https://blog.trailofbits.com/2020/06/11/ecdsa-handle-with-care/
    # recover nonce
    # recover secret key
    aes_key = None
    
    # decrypt ciphertext
    iv = b"\x00" * 16
    ciphertext = bytes.fromhex("f3ccfd5877ec7eb886d5f9372e97224c43f4412ca8eaeb567f9b20dd5e0aabd5")

    aes = AES.new(aes_key, AES.MODE_CBC, iv)
    plaintext = aes.decrypt(ciphertext)

    print(f"Flag: {plaintext}")


if __name__ == "__main__":
    main()