# Easy Peasy (50)
Find the flag here:

`NzMgNzkgNmUgNzQgN2IgNzAgNjIgNjEgNzQgNjUgNmUgNjcgNjYgNWYgNmMgNjIgNjggNWYgNzQgNjIgNjcgNWYgN2EgNzIgN2Q=`

**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/tenable/crypto/easy_peasy/)**