import string
import time
from pwn import *

def main():
    # flag{f0ll0w_th3_whit3_r@bb1t}
    flag = b"flag{"

    while flag[-1] != b"}":
        for c in string.printable:
            conn = remote("challenges.ctfd.io", 30468)
            conn.recvuntil(b"[flag]>").decode("utf-8")
            guessing = flag + c.encode("utf-8")
            conn.sendline(guessing)
            response = conn.recvall().decode("utf-8")
            if "Flag does not start" not in response:
                flag = guessing
                conn.close()
                print(flag)
                break
            conn.close()
            time.sleep(1)

if __name__ == "__main__":
    main()