# Send A Letter (50)
There is a web app at http://challenges.ctfd.io:30471/. Find the vulnerability, exploit it, and recover the flag.

**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/tenable/web/letter/)**