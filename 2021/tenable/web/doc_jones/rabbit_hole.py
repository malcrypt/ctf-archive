import re
import requests
import time

def main():
    proxies = {"http": "http://127.0.0.1:8080"}
    host = "167.71.246.232"
    port = 8080
    next_page = "cE4g5bWZtYCuovEgYSO1"
    indices = []
    image_bytes = []

    while next_page:
        response = requests.get(f"http://{host}:{port}/rabbit_hole.php", params={"page": next_page}, proxies=proxies)
        match = re.match(r"^\[(.+), \'(.+)\'\]\s+(.+)$", response.text)
        if match:
            indices.append(int(match.group(1)))
            image_bytes.append(int(match.group(2), 16))
            next_page = match.group(3)
        else:
            print("End of the rabbit hole!")
            next_page = None

        time.sleep(.5)

    image_data = bytearray(max(indices) + 1)

    for i, b in enumerate(image_bytes):
        image_data[indices[i]] = b

    with open("output.png", "wb") as f:
        f.write(image_data)

if __name__ == "__main__":
    main()