import glob
import os
import shutil
import tempfile
import zipfile


def main():
    temp_dir = tempfile.mkdtemp()
    layer = 128

    shutil.copy("turtles128.zip", os.path.join(temp_dir, "turtles128.zip"))
    passwords = []

    while layer > 0:
        next_file = os.path.join(temp_dir, f"turtles{layer}.zip")
        with zipfile.ZipFile(next_file) as zf:
            for i in range(1000):
                try:
                    zf.extractall(path=temp_dir, pwd=str(i).encode("utf-8"))
                    passwords.append(str(i))
                    break
                except:
                    pass

        layer -= 1

    for filename in glob.glob(os.path.join(temp_dir, "turtle*.zip")):
        os.remove(filename)

    values = []
    for i in range(0, len(passwords), 8):
        values.append(int("".join(passwords[i:i+8]), 2))

    ciphertext = bytes(values).hex()

    print("Ciphertext: " + ciphertext)


if __name__ == "__main__":
    main()
