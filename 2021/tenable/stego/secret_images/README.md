# Secret Images (125)
We discovered Arasaka operatives sending weird pictures to each other. It looks like they may contain secret data. Can you find it?

[crypted1.png](provided/crypted1.png)

[crypted2.png](provided/crypted2.png)

**[Solution](https://malcrypt.gitlab.io/blog/ctfs/2021/tenable/stego/secret_images/)**
